
$(function(){

	CKEDITOR.config.disableNativeSpellChecker = false;
	CKEDITOR.config.scayt_autoStartup = false;
	 CKEDITOR.config.removePlugins = 'scayt,checkspell';

	fckconfig_common = {
			// extraPlugins : 'stylesheetparser',
			extraPlugins: 'indent',

			skin: 'BootstrapCK-Skin',
			 contentsCss: [edytor_css2, edytor_css],
			 bodyClass : 'page-text',
			//EditorAreaCSS: edytor_css,
			stylesSet: [
			     { name : 'Normalny', element : 'p', attributes : { 'class' : '' } },
			     { name : 'Zwykły', element : 'span', attributes : { 'class' : '' } },
			     { name : 'Pogrubinie', element: 'span', attributes: {'class' : 'pogrubienie'}},
			     { name : 'Pogrubinie 2', element: 'span', attributes: {'class' : 'pogrubienie2'}},
			     //{ name : 'Akapit wyróżniony', element : 'p', attributes : { 'class' : 'wyroznienie' } },
			     //{ name : 'Akapit wyróżniony 2', element : 'p', attributes : { 'class' : 'wyroznienie2' } },
			     { name : 'Akapit wcięcie 1 wiersza', element : 'p', attributes : { 'class' : 'wciecie' } },
			     //{ name : 'Wypunktowanie', element : 'ul', attributes : { 'class' : 'wypunktowanie1' } },
			     { name : 'Lista (1, 1, 2, ...)', element: 'ol', attributes: {'class' : ''}},
			     { name : 'Lista (1.1, 1.2, ...)', element: 'ol', attributes: {'class' : 'numeracja-rozdzialy'}},
			     //{ name: 'Link - Plik do pobrania', element: 'a', attributes: {'class' : 'do-pobrania' } }
			],

			 filebrowserBrowseUrl : edytor_browse_url,

			 // filebrowserUploadUrl : '',

	};



	fckconfig = jQuery.extend(true,  {

	}, fckconfig_common);

	fckconfig_basic = jQuery.extend(true, {

			toolbar:
				[
					['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
					['UIColor']
				]

	}, fckconfig_common );

	fckconfig_tiny = jQuery.extend(true, {

		toolbar:
			[
				['Source', '-',  'Bold', 'Italic', 'Strike', '-', 'TextColor', 'FontSize' ],
				[ 'NumberedList', 'BulletedList'],
				['Link' , 'Unlink', 'Anchor', 'Image']

			]

	}, fckconfig_common );


	$('.wysiwyg').ckeditor(fckconfig);
	$('.wysiwyg-basic').ckeditor(fckconfig_basic);
	$('.wysiwyg-tiny').ckeditor(fckconfig_tiny);





});