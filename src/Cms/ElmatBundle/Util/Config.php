<?php

namespace Cms\ElmatBundle\Util;

use Cms\ElmatBundle\Entity\Setting;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;


class Config {

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var EntityRepository
	 */
	protected $repo;

	protected $settings = array();

	protected $is_loaded = false;

	protected $default_settings = array(

	        'ust_zmieniarka_strona_glowna_interwal' => array(
	                'value' => '4000',
	                'description' => 'Interwał dla głównej zmieniarki zdjęć, jednostka: [ms]' ),

	        'seo_title' => array(
	                'value' => 'Elmat',
	                'description' => 'Domyślny tytuł strony - strona główna - wartość meta tagu title'),

	        'seo_description' => array(
	                'value' => 'Strona Elmat.pl',
	                'description' => 'Opis strony - strona główna - wartość meta tagu "description"'),

	        'seo_keywords' => array(
	                'value' => 'Elmat, Elmat.pl',
	                'description' => 'Domyślne słowa kluczowe - strona główna - wartość meta tagu "keywords"'),

            // structural_cabling
	        'seo_title_structural_cabling' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Domyślny tytuł strony - strona Structural Cabling - wartość meta tagu title'),
            'seo_description_structural_cabling' => array(
                    'value' => 'Strona Elmat.pl',
                    'description' => 'Opis strony - strona Structural Cabling - wartość meta tagu "description"'),
            'seo_keywords_structural_cabling' => array(
                    'value' => 'Elmat, Elmat.pl',
                    'description' => 'Domyślne słowa kluczowe - strona Structural Cabling - wartość meta tagu "keywords"'),


            // fiber_optic
	        'seo_title_fiber_optic' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Domyślny tytuł strony - strona Fiber Optic- wartość meta tagu title'),
            'seo_description_fiber_optic' => array(
                    'value' => 'Strona Elmat.pl',
                    'description' => 'Opis strony - strona Fiber Optic - wartość meta tagu "description"'),
            'seo_keywords_fiber_optic' => array(
                    'value' => 'Elmat, Elmat.pl',
                    'description' => 'Domyślne słowa kluczowe - strona Fiber Optic - wartość meta tagu "keywords"'),


            // fttx_systems
	        'seo_title_fttx_systems' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Domyślny tytuł strony - strona Fttx Systems- wartość meta tagu title'),
            'seo_description_fttx_systems' => array(
                    'value' => 'Strona Elmat.pl',
                    'description' => 'Opis strony - strona Fttx Systems - wartość meta tagu "description"'),
            'seo_keywords_fttx_systems' => array(
                    'value' => 'Elmat, Elmat.pl',
                    'description' => 'Domyślne słowa kluczowe - strona Fttx Systems - wartość meta tagu "keywords"'),


            // passive_optical
	        'seo_title_passive_optical' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Domyślny tytuł strony - strona Passive Optical- wartość meta tagu title'),
            'seo_description_passive_optical' => array(
                    'value' => 'Strona Elmat.pl',
                    'description' => 'Opis strony - strona Passive Optical - wartość meta tagu "description"'),
            'seo_keywords_passive_optical' => array(
                    'value' => 'Elmat, Elmat.pl',
                    'description' => 'Domyślne słowa kluczowe - strona Passive Optical - wartość meta tagu "keywords"'),

            // active_elements
	        'seo_title_active_elements' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Domyślny tytuł strony - strona Active Elements- wartość meta tagu title'),
            'seo_description_active_elements' => array(
                    'value' => 'Strona Elmat.pl',
                    'description' => 'Opis strony - strona Active Elements - wartość meta tagu "description"'),
            'seo_keywords_active_elements' => array(
                    'value' => 'Elmat, Elmat.pl',
                    'description' => 'Domyślne słowa kluczowe - strona Active Elements - wartość meta tagu "keywords"'),

            // connectivity
	        'seo_title_connectivity' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Domyślny tytuł strony - strona Connectivity- wartość meta tagu title'),
            'seo_description_connectivity' => array(
                    'value' => 'Strona Elmat.pl',
                    'description' => 'Opis strony - strona Connectivity - wartość meta tagu "description"'),
            'seo_keywords_connectivity' => array(
                    'value' => 'Elmat, Elmat.pl',
                    'description' => 'Domyślne słowa kluczowe - strona Connectivity - wartość meta tagu "keywords"'),

	        // connectivity
	        'seo_title_live' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Domyślny tytuł strony - strona Live Elmat- wartość meta tagu title'),
            'seo_description_live' => array(
                    'value' => 'Strona Elmat.pl',
                    'description' => 'Opis strony - strona Live Elmat - wartość meta tagu "description"'),
            'seo_keywords_live' => array(
                    'value' => 'Elmat, Elmat.pl',
                    'description' => 'Domyślne słowa kluczowe - strona Live Elmat - wartość meta tagu "keywords"'),

                'txt_stopka_copyrights' => array(
	                'value' => '(c) 2013 P.H. ELMAT Sp. z o.o.<br/> Wszelkie prawa zastrzeżone',
	                'description' => 'Tekst copyrights w stopce - html'),


	        'txt_stopka_link_facebook' => array(
	                'value' => 'http://www.facebook.com/',
	                'description' => 'Link do fanpage Facebook w stopce - jeżeli pusty - nie zostanie wyświetlony link'),

	        'txt_stopka_link_youtube' => array(
	                'value' => 'http://www.youtube.com/',
	                'description' => 'Link do kanału Youtube w stopce - jeżeli pusty - nie zostanie wyświetlony link'),

	        'txt_strefa_klienta_logowanie' => array(
	                'value' => 'Jeśli jesteś naszym Klientem i posiadasz konto w systemie zaloguj się poniżej używając swojego loginu i hasła.',
	                'description' => 'Tekst nad formularzem logowania do strefy klienta'
	        ),

	        'txt_strefa_klienta_rejestracja' => array(
	                'value' => 'Jeśli nie posiadasz konta w naszym systemie zarejestruj się. Posiadanie konta w systemie ma wiele zalet - otrzymasz dostęp do cenników, będziesz informowany o nowościach, zbliżających się szkoleniach, promocjach i innych ważnych wydarzeniach.',
	                'description' => 'Tekst nad przyciskiem Zarejestru się'
	        ),

	        'txt_strefa_klienta_rejestracja_krok1' => array(
	                'value' => 'Dziękujemy za rozpoczecie procesu rejestracji. Uprzejmie prosimy o wprowadzenei danych do logowania, pełnych danych firmowych oraz o uzupełnienia zakresu zainteresowań w celu otrzymywania odpowiednich powiadomień.',
	                'description' => 'Tekst nad formularzem rejestracji krok 1'
	         ),
	        'txt_strefa_klienta_rejestracja_krok2' => array(
	                'value' => 'Prosimy o sprecyzowanie obszarów działalności Państwa firmy oraz określenie zainteresowania naszą ofertą.',
	                'description' => 'Tekst nad formularzem rejestracji krok 2'
	        ),
	        'txt_strefa_klienta_rejestracja_krok3' => array(
	                'value' => 'Sprawdź czy podane przez Ciebie informacje są poprawne. Jeżeli wszystko się zgadza potwierdź zgłoszenie.',
	                'description' => 'Tekst nad podsumowaniem rejestracji klienta'
	        ),

	        'txt_strefa_klienta_rejestracja_zgoda' => array(
	                'value' =>
            	        'Wyrażam zgodę na przetwarzanie przez firmę ELMAT Sp.z o.o.,
            	        Rzeszów ul. Wspólna 4a, dotyczącego mnie adresu poczty elektronicznej,
            	        w myśl przepisów ustawy z dnia 29 sierpnia 1997r. o ochronie danych osobowych
            	        (Dz. U. z 2002r. Nr 101, poz. 926 z póź. zm.) w zakresie prowadzonej przez ELMAT działalności gospodarczej
            	        (marketingowej) oraz zgodnie z Ustawą z dnia 18.07.2002 r. (Dz. U. nr 144, poz.1204) o świadczeniu usług drogą elektroniczną,
            	        która weszła w życie 10.03.2003 r., wyrażam zgodę na otrzymywanie poczty droga elektroniczną.',
	                'description' => 'Tekst zgody na przetwarzanie danych'

	        ),

	        'txt_strefa_klienta_rejestracja_podziekowanie' => array(
	                'value' => 'Dziękujemy za rejestrację. Na adres e-mail przyjdzie wiadomość potwierdzająca dokonaną rejestrację.',
	                'description' => 'Tekst podziękowania za rejestrację wyświetlany na stronie '
	        ),

	        'txt_strefa_klienta_rejestracja_potwierdzenie_email' => array(
	                'value' => 'Twój e-mail został potwierdzony i konto zostało aktywowane. Aby uzyskać dostęp do cenników wymagana jest dodatkowa akceptacja Administratora',
	                'description' => 'Tekst informacji o potwierdzeniu konta przez klienta'
	        ),
        
            'txt_strefa_klienta_rejestracja_potwierdzenie_email_link_wykorzystany' => array(
	                'value' => 'Link aktywacyjny został już użyty. Twój e-mail został już wcześniej potwierdzony i konto zostało aktywowane. ',
	                'description' => 'Tekst informacji gdy klient skorzystał z linku aktywacyjnego ponownie.'
	        ),
        
            'txt_strefa_klienta_przypomnienie' => array(
	                'value' => 'Podaj adres E-mail podany przy rejestracji na który zostanie przesłana instrukcja zresetowania hasła.',
	                'description' => 'Tekst informacji nad formularzem resetowania hasła'
	        ),
        
            'txt_strefa_klienta_przypomnienie_zmiana_hasla' => array(
	                'value' => 'Poniżej możesz ustawić nowe hasło do swojego konta.',
	                'description' => 'Tekst informacji nad formularzem resetowania hasła (zmiana hasła)'
	        ),


	        'ust_mailer_wiadomosc_od' => array(
	                'value' => 'Elmat.pl',
	                'description' => 'Nazwa pojawiająca się w polu "OD" w mailach wysyłanych przez system'
	         ),

	        'ust_rejestracja_potwierdzenie_kopia_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść kopia wiadomości wysłanej do klienta podczas rejestracji'
             ),

	        'ust_rejestracja_powiadomienie_nowe_konto' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść powiadomienie o nowym koncie klienta (które klient potwierdził)'
	        ),


	        'txt_formularz_kontaktowy_wstep' => array(
	                'value' => '',
	                'description' => 'Treść wstępu nad domyślnym formularzem kontaktowym'
	        ),
	        'formularz_wysylka_email' => array(
	                'value' =>
'centrala,Centrala,elmat@elmat.pl
lodz,Oddział Łódź,lodz@elmat.pl
warszawa,Oddział Warszawa,warszawa@elmat.pl
katowice,Oddział Katowice,katowice@elmat.pl
krakow,Oddział Kraków,krakow@elmat.pl
ksiegowosc,Księgowość,ksiegowosc@elmat.pl
export,Export,export@elmat.pl
magazyn,Magazyn,magazyn@elmat.pl
serwis,Serwis,serwis@elmat.pl
logistyka,Logistyka i dostawy,logistyka@elmat.pl
laboratorium,Laboratorium,laboratorium@elmat.pl
dwp,Dział Wsparcia Projektowego,dwp@elmat.pl
it,Dział IT i Marketingu,it@elmat.pl',

	                'description' => 'Adresy na które wysyłane są wiadomości z formularza kontaktowego w formacie: symbol,nazwa,adres/adresy email oddzielone przecinkami'
	        ),


	        'formularz_wysylka_temat' => array(
	                'value' => 'Nowa wiadomość z formularza kontaktowego',
	                'description' => 'Domyślny temat wiadomości przesyłanej przez formularz kontaktowy'
	        ),

	        'formularz_wysylka_od' => array(
	                'value' => 'Formularz kontaktowy',
	                'description' => 'Pole nadawca dla wiadomości przesyłanej przez formularz kontaktowy'
	        ),

	        'txt_formularz_podziekowanie' => array(
	                'value'=> 'Wiadomość została wysłana. Dziękujemy za skorzystanie z formularza.',
	                'description' => 'Podziękowanie za skorzystanie z formularza kontaktowego'
	        ),

	        'ust_box_strona_glowna' => array(
	                'value' => 'strona-glowna-1,strona-glowna-2',
	                'description' => 'Lista box-ów (slug) wyświetlanych na stronie głównej oddzielona przecinkami'
	        ),

	        'ust_newsletter_wysylka_maks_ilosc' => array(
	                'value' => '100',
	                'description' => 'Maksymalna liczba adresów email na które wysyłany jest newsletter podczas pojedynczego uruchomienia wysyłki (limit obowiązuje przy Cron i ręcznym uruchomieniu)'
	        ),

	        'ust_newsletter_wysylka_min_czas' => array(
	                'value' => '300',
	                'description' => 'Minimalny czas  (limit w sek.) pomiędzy kolejnymi uruchomieniami wysyłki newslettera'
	        ),

	        'ust_newsletter_wysylka_od' => array(
	                'value' => 'Newsletter Elmat.pl',
	                'description' => 'Pole nadawca dla wiadomości przesyłanej przez newsletter'
	        ),



	        'ust_newsletter_aktywacja_kopia_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść kopia wiadomości wysłanej do klienta w celu aktywacji subskrypcji newslettera'
             ),

	        'ust_newsletter_nowa_subskrypcja_komunikat' => array(
	                'value' => 'Na twój e-mail został wysłany link aktywacyjny.',
	                'description' => 'Treść komunikatu z informacją o konieczności potwierdzenia adresu email subskrypcji newslettera'
	        ),

	        'ust_newsletter_aktywacja_komunikat' => array(
	                'value' => 'Dziękujemy za aktywację subskrypcji',
	                'description' => 'Treść komunikatu po potwierdzeniu adresu email subskrypcji newslettera'
	        ),

	        'ust_newsletter_aktywacja_komunikat2' => array(
	                'value' => 'Subskrypcja jest już aktywna',
	                'description' => 'Treść komunikatu po potwierdzeniu adresu email subskrypcji newslettera - w przypadku gdy już wcześniej subskrypcja została aktywowana'
	        ),

	        'ust_newsletter_dezaktywacja_komunikat' => array(
	                'value' => 'Twój adres E-mail został wypisany z newslettera',
	                'description' => 'Treść komunikatu po wyłączeniu subskrypcji przez klienta'
	        ),
        
            'ust_cookies_tytul' => array(
	                'value' => 'Pliki cookies w naszym serwisie.',
	                'description' => 'Tytył komunikatu o cookies'
	        ),
            'ust_cookies_komunikat' => array(
	                'value' => 'Informacji zarejestrowanych w plikach "cookies" używamy m.in. w celach reklamowych
                                i statystycznych oraz w celu dostosowania naszych serwisów do indywidualnych potrzeb użytkowników.
                                Możesz zmienić ustawienia dotyczące "cookies" w swojej przeglądarce internetowej.
                                Jeżeli pozostawisz te ustawienia bez zmian pliki cookies zostaną zapisane w pamięci urządzenia.
                                Zmiana ustawień plików "cookies" może ograniczyć funkcjonalność serwisu.',
	                'description' => 'Tytył komunikatu o cookies'
	        ),
            'ust_cookies_zgoda_link' => array(
	                'value' => 'Nie pokazuj więcej tego komunikatu.',
	                'description' => 'Treść linku do zamknięcia komunikatu o cookies'
	        ),
        
            'txt_szkolenia_rejestracja_krok_logowanie' => array(
	                'value' => 'W celu zapisania się na szkolenia oferowane przez Akademię Fibrain prosimy o zalogowanie się lub uzupełnienie formularza rejestracyjnego. Otrzymacie Państwo e-maila potwierdzającego przyjęcie zgłoszenia.',
	                'description' => 'Tekst nad formularzem rejestracji - potrzeba zalogowania'
	         ),
        
            
        
            'txt_szkolenia_rejestracja_krok1' => array(
	                'value' => 'Prosimy o podanie dokładnych danych uczestników planujących przystąpienie do szkolenia. Informacje te pozwolą nam przygotować odpowiednią ilość materiałów szkoleniowych.',
	                'description' => 'Tekst nad formularzem rejestracji krok 1'
	         ),
            'txt_szkolenia_rejestracja_krok2' => array(
	                'value' => 'Sprawdź czy podane przez Ciebie informacje są poprawne. Jeśli wszystko się zgadza <u>prosimy o wydrukowanie podsumownaia i wysłania do faxem pod numer (+48) 17 866 08 10.</u>',
	                'description' => 'Tekst nad formularzem rejestracji krok 2'
	         ),
        
            'ust_rejestracja_szkolenie_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami adresy e-mail na które ma przyjść powiadomienie o nowej rejestracji na szkolenie'
             ),
            
          


    );



	public function __construct(EntityManager $em) {

		$this->em = $em;

	}


	/**
	 * @param string $name Name of the setting.
	 * @return string|null Value of the setting.
	 * @throws \RuntimeException If setting is not defined.
	 */
	public function get($name, $default = '') {

	    $this->all();

		if(array_key_exists($name, $this->settings)) {
			return $this->settings[$name];
		}

		$sett = new Setting();
		$sett->setName($name);
		$sett->setValue($default);
		$this->em->persist($sett);
		$this->em->flush();

		$this->settings[$name] = $default;

		return $default;

	}

	/**
	 * @return string[] with key => value
	 */
	public function all() {

		$settings = array();

		if($this->is_loaded) {
			return $this->settings;
		}

		foreach ($this->getRepo()->findAll() as $setting) {
			$settings[$setting->getName()] = $setting->getValue();
		}


		foreach($this->default_settings as $_name => $_def_setting) {
            if(!array_key_exists($_name, $settings)) {
                $sett = new Setting();
                $sett->setName($_name);
                $sett->setValue($_def_setting['value']);
                $sett->setDescription($_def_setting['description']);
                $this->em->persist($sett);
                $this->em->flush();
                $settings[$_name] = $_def_setting['value'];
            }
		}

		$this->settings = $settings;
		$this->is_loaded = true;;

		return $settings;
	}

	/**
	 * @return EntityRepository
	 */
	protected function getRepo() {
		if ($this->repo === null) {
			$this->repo = $this->em->getRepository(get_class(new Setting()));
		}

		return $this->repo;
	}

}
