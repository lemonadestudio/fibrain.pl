<?php

namespace Cms\ElmatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cms\ElmatBundle\Entity\NewsletterSubscriber;

class ImportSubscribersCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('newsletter:import-subscribers')
            ->setDescription('Import subskrynentów newslettera ze starej tabeli "subscription".');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $importedSubscribersCount = 0;
        
        $existingSubscribers = $em->createQueryBuilder('s')
                ->select('s')
                ->from('CmsElmatBundle:Subscription', 's')
                ->orderBy('s.id', 'ASC')
                ->getQuery()
                ->execute();
        
        foreach ($existingSubscribers as $existingSubscriber) {
            $subscriber = new NewsletterSubscriber();
            $subscriber->setEmail($existingSubscriber->getEmail());
            $subscriber->setToken($existingSubscriber->getConfirmationToken());
            $subscriber->setIsActive($existingSubscriber->getStatus() === 1);
            $subscriber->setCreatedAt($existingSubscriber->getCreated());
            $subscriber->setUpdatedAt($existingSubscriber->getUpdated());
            $em->persist($subscriber);
            $importedSubscribersCount++;
        }
        
        $em->flush();
        $output->writeln('Zaimportowano : ' . $importedSubscribersCount . ' subskrybentów.');
    }
    
}
