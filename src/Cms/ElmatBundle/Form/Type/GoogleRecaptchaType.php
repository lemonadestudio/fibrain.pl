<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GoogleRecaptchaType extends AbstractType {
    private $container;
    private $siteKey;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->siteKey = $this->container->get('cms_config')->get('google_recaptcha_site_key');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'site_key' => $this->siteKey,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['site_key'] = $options['site_key'];
    }

    public function getParent() {
        return 'form';
    }

    public function getName() {
        return 'google_recaptcha';
    }
}
