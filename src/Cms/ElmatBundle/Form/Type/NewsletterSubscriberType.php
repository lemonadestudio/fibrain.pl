<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewsletterSubscriberType extends AbstractType {

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $zgoda_no_blank = new NotBlank();
        $zgoda_no_blank->message = 'Należy wyrazić zgodę';

        $builder->add('email', 'text', array(
            'required' => true,
            'constraints' => array(
                new NotBlank(),
                new Email(),
            )
        ));
        /*
        $builder->add('zgoda_przetwarzanie', 'checkbox', array(
            'required' => true,
            'mapped' => false,
            'constraints' => array($zgoda_no_blank)
        ));
        $builder->add('zgoda_otrzymywanie', 'checkbox', array(
            'required' => true,
            'mapped' => false,
            'constraints' => array($zgoda_no_blank)
        ));
        */
        $builder->add('zgoda_przesylanie', 'checkbox', array(
            'required' => true,
            'mapped' => false,
            'constraints' => array($zgoda_no_blank)
        ));
    }

    public function getName() {
        return 'NewsletterSubscriber';
    }

}
