<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\AbstractType;
use Cms\ElmatBundle\Validator\Constraints\GoogleRecaptcha;

class ContactZapytanieType extends AbstractType {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $formularz_wysylka_email = $this->container->get('cms_config')->get('formularz_wysylka_email');
        $typ_choices = array();

        foreach (explode("\n", $formularz_wysylka_email) as $cfg_email_line) {
            $cfg_email = array_map('trim', explode(',', $cfg_email_line));
            // $cfg_email = explode(',', $cfg_email_line);
            if (count($cfg_email) >= 3) {
                $typ_choices[trim($cfg_email[0])] = trim($cfg_email[1]);
            }
        }

        $builder->add('typ', 'choice', array(
            'choices' => $typ_choices,
            'label' => 'Kontakt z'
        ));
        $builder->add('firma', 'text', array(
            'required' => false,
            'label' => 'Firma',
            'constraints' => array(
                new Length(array(
                    'max' => 100
                ))
            )
        ));
        $builder->add('imie_nazwisko', 'text', array(
            'label' => 'Imię i nazwisko',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Imię: Uzupełnij to pole'
                )),
                new Length(array(
                    'max' => 100
                ))
            )
        ));
        $builder->add('email', 'text', array(
            'label' => 'E-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Email: Uzupełnij to pole'
                )),
                new Email(array(
                    'message' => 'Email: Nieprawidłowy adres e-mail'
                ))
            )
        ));
        $builder->add('telefon', 'text', array(
            'required' => false,
            'label' => 'Telefon',
            'constraints' => array(
                new Length(array(
                    'max' => 100
                ))
            )
        ));
        $builder->add('tresc', 'textarea', array(
            'label' => 'Wiadomość',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Treść: Uzupełnij to pole'
                )),
                new Length(array(
                    'max' => 100
                ))
            )
        ));
        $builder->add('google_recaptcha', new GoogleRecaptchaType($this->container), array(
            'label' => 'Ochrona antyspamowa',
            'error_bubbling' => false,
            'constraints' => array(
                new GoogleRecaptcha()
            )
        ));
        $builder->add('g-recaptcha-response', null, array(
            'mapped' => false
        ));

        /*
        $builder->add('zgoda_przetwarzanie_first', 'checkbox', array(
            'required' => true,
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Należy wyrazić zgodę'
                ))
            )
        ));
        $builder->add('zgoda_przetwarzanie_second', 'checkbox', array(
            'required' => true,
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Należy wyrazić zgodę'
                ))
            )
        ));
        $builder->add('zgoda_otrzymywanie', 'checkbox', array(
            'required' => false,
            'mapped' => false
        ));
        $builder->add('zgoda_przesylanie', 'checkbox', array(
            'required' => false,
            'mapped' => false
        ));*/
    }

    public function getName() {
        return 'contact_zapytanie';
    }
}
