<?php

namespace Cms\ElmatBundle\Extensions;

use Cms\ElmatBundle\Util\Config;

class SettingsExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface {
    protected $cms_config;

    function __construct(Config $config) {
        $this->cms_config = $config;
    }

    public function getGlobals() {
        return array(
            'settings' => $this->cms_config->all(),
            'cms_config' => $this->cms_config
        );
    }

    public function getName() {
        return 'cms_config';
    }

}