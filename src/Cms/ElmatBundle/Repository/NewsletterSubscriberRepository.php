<?php

namespace Cms\ElmatBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Cms\ElmatBundle\Model\OfferType;

/**
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NewsletterSubscriberRepository extends EntityRepository {

    public function getAllAsArray()
    {
        return $this->getEntityManager()
                ->createQueryBuilder('c')
                ->select('c.id, c.email')
                ->from('CmsElmatBundle:NewsletterSubscriber', 'c')
                ->orderBy('c.id', 'ASC')
                ->getQuery()
                ->getArrayResult();
    }
    
    public function getSubscriberIds($chosenEmails, $onlySubscribersWithAccount, $offerTypes)
    {
        $qb = $this->getEntityManager()
                ->createQueryBuilder('s')
                ->select('s.id')
                ->from('CmsElmatBundle:NewsletterSubscriber', 's');
        
        $qb = $this->addOnlySubscribersWithAccountQuery($onlySubscribersWithAccount, $offerTypes, $qb);
        $qb = $this->addChosenEmailsQuery($chosenEmails, $qb);
        
        return $qb
                ->andWhere('s.isActive = true')
                ->getQuery()
                ->execute();
    }
    
    private function addChosenEmailsQuery($chosenEmails, $queryBuilder) 
    {
        if (count($chosenEmails) <= 0) {
            return $queryBuilder;
        }
        
        return $queryBuilder
                ->andWhere('s.email IN (:emails)')
                ->setParameter('emails', $chosenEmails);
    }
    
    private function addOnlySubscribersWithAccountQuery($onlySubscribersWithAccount, $offerTypes, $queryBuilder) {
        if (!$onlySubscribersWithAccount) {
            return $queryBuilder;
        }
        
        $innerJoinUserExpression = $queryBuilder->expr()->eq('s.email', 'u.email');
        
        if (count($offerTypes) > 0) {
            $offerTypeExpression = $queryBuilder->expr()->orX();

            foreach ($offerTypes as $offerType) {
                $userColumn = OfferType::getMappedUserOfferColumn($offerType->getType());

                if (!$userColumn) {
                    continue;
                }

                $offerTypeExpression->add($queryBuilder->expr()->eq("u.{$userColumn}", 'true'));
            }

            $innerJoinUserExpression = $queryBuilder->expr()->andX($innerJoinUserExpression, $offerTypeExpression);
        }
        
        return $queryBuilder
                ->innerJoin('CmsElmatBundle:User', 'u', 'WITH', $innerJoinUserExpression)
                ->distinct('u.email');
    }
}
