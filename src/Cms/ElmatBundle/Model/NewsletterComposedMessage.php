<?php

namespace Cms\ElmatBundle\Model;

class NewsletterComposedMessage {
    private $title;
    private $htmlMessage;
    private $plainTextMessage;
    private $recipientMail;
    
    public function __construct($title, $htmlMessage, $plainTextMessage, $recipientMail) {
        $this->title = $title;
        $this->htmlMessage = $htmlMessage;
        $this->plainTextMessage = $plainTextMessage;
        $this->recipientMail = $recipientMail;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getHtmlMessage() {
        return $this->htmlMessage;
    }
    
    public function getPlainTextMessage() {
        return $this->plainTextMessage;
    }
    
    public function getRecipientMail() {
        return $this->recipientMail;
    }
}
