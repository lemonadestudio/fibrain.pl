<?php

namespace Cms\ElmatBundle\Model;

/**
 * reprezentuje dane z konfiguracji cms_elmat
 *
 */
class Domain {
    private $symbol;
    private $domain;
    private $description;
    private $languages;
    private $is_main;

    public function __construct($data) {
        $this->symbol = $data['symbol'];
        $this->domain = $data['domain'];
        $this->description = $data['description'];
        $this->languages = $data['languages'];
        $this->is_main = $data['is_main'];
    }

    public function getSymbol() {
        return $this->symbol;
    }

    public function getDomain() {
        return $this->domain;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getLanguages() {
        return $this->languages;
    }

    public function isMain() {
        return $this->is_main;
    }
}