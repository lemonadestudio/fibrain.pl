<?php

namespace Cms\ElmatBundle\Model;

use Cms\ElmatBundle\Model\NewsletterMessageRecipientStatus;
use Cms\ElmatBundle\Model\NewsletterComposedMessage;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NewsletterMessageComposer {
    private $container;
    private $recipient;
    private $user;
    private $userManager;
    private $error;
    private $router;
    
    public function __construct($recipient, $container) {
        $this->container = $container;
        $this->recipient = $recipient;
        $this->user = null;
        $this->userManager = $this->container->get('fos_user.user_manager');
        $this->router = $this->container->get('router');
    }
    
    public function setMatchingUser($users) {
        if (!$users || count($users) === 0) {
            return;
        }
        
        foreach ($users as $user) {
            if ($user->getEmail() === $this->recipient->getNewsletterSubscriber()->getEmail()) {
                $this->user = $user;
                break;
            }
        }
    }
    
    public function canCompose() {
        if (!$this->isValidEmail($this->recipient->getNewsletterSubscriber()->getEmail())) {
            $this->error = NewsletterMessageRecipientStatus::INVALID_EMAIL;
            
            return false;
        }
        
        if ($this->recipient->getNewsletterMessage()->getSendOnlyToSubscribersWithAccount() &&
            !$this->user) {
            $this->error = NewsletterMessageRecipientStatus::NO_ACCOUNT;
            
            return false;
        }
        
        return true;
    }
    
    public function getError() {
        return $this->error;
    }
    
    public function composeMessage() {
        if (!$this->canCompose()) {
            return null;
        }
        
        if ($this->recipient->getNewsletterMessage()->getSendOnlyToSubscribersWithAccount()) {
            return $this->composeMessageForRecipientWithUserAccount();
        } else {
            return $this->composeMessageForRecipient();
        }
    }
    
    private function isValidEmail($email) {
        $constraints = array(
            new \Symfony\Component\Validator\Constraints\Email(),
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );

        $errorList = $this->container
                ->get('validator')
                ->validateValue($email, $constraints);

        return count($errorList) == 0;
    }
    
    private function composeMessageForRecipient() {
        $messageVariables = array(
            '{URL_REZYGNACJA}' => $this->router->generate(
                'cms_elmat_newsletter_unsubscribe', array(
                    'id' => $this->recipient->getNewsletterSubscriber()->getId(),
                    'token' => $this->recipient->getNewsletterSubscriber()->getToken()
                ), true
            )
        );
        
        $htmlMessage = $this->getMessageWithFilledVariables(
                $messageVariables,
                $this->recipient->getNewsletterMessage()->getMessage());
        
        $plainTextMessage = $this->getMessageWithFilledVariables(
                $messageVariables,
                $this->recipient->getNewsletterMessage()->getPlainTextMessage());
        
        
        
        return new NewsletterComposedMessage(
                $this->recipient->getNewsletterMessage()->getTitle(),
                $htmlMessage,
                $plainTextMessage,
                $this->recipient->getNewsletterSubscriber()->getEmail());
    }
    
    private function composeMessageForRecipientWithUserAccount() {
        if (!$this->user) {
            return null;
        }
        
        $messageVariables = $this->createMessageVariablesForRecipientWithUserAccount($this->user);
            
        $htmlMessage = $this->getMessageWithFilledVariables(
            $messageVariables,
            $this->recipient->getNewsletterMessage()->getMessage());

        $plainTextMessage = $this->getMessageWithFilledVariables(
            $messageVariables,
            $this->recipient->getNewsletterMessage()->getPlainTextMessage());

        return new NewsletterComposedMessage(
            $this->recipient->getNewsletterMessage()->getTitle(),
            $htmlMessage,
            $plainTextMessage,
            $this->recipient->getNewsletterSubscriber()->getEmail());
    }
    
    private function getMessageWithFilledVariables($messageVariables, $message) {
        return str_replace(
                array_keys($messageVariables),
                array_values($messageVariables), 
                $message);
    }
    
    private function createMessageVariablesForRecipientWithUserAccount($user) {
        $userResetToken = $user->getPasswordResetToken();
            
        if (!$userResetToken) {
            $userResetToken = $this->createNewUserResetToken($user);
        }

        return array(
            '{URL_ZMIANA_HASLA}' => $this->router->generate(
                'cms_elmat_userzone_reset_password_change', 
                array(
                    'id' => $user->getId(), 
                    'token' => $userResetToken
                ), UrlGeneratorInterface::ABSOLUTE_URL),
            '{FIRMA}' => $user->getFirma(),
            '{NIP}' => $user->getNip(),
            '{EMAIL}' => $user->getEmail(),
            '{TELEFON}' => $user->getTelefon(),
            '{ULICA}' => $user->getUlica(),
            '{KOD_POCZTOWY}' => $user->getKodPocztowy(),
            '{MIASTO}' => $user->getMiasto()
        );
    }
    
    private function createNewUserResetToken($user) {
        $userResetToken = sha1(uniqid($user->getId(), true));
        $user->setPasswordResetToken($userResetToken);
        $this->userManager->updateUser($user, true);
    }
}
