<?php

namespace Cms\ElmatBundle\Model;

class NewsletterMessageRecipientStatus {
    // gotowy do wyslania
    const READY_TO_SEND = 1;
    
    // wyslany
    const SENT = 2;
    
    // nieprawidlowy email
    const INVALID_EMAIL = 3;
    
    // po dodaniu newslettera a przed jego wyslaniem, ktos zmienil maila
    // wizacego uzytkownika z subskrybentem
    const NO_ACCOUNT = 4;
    
    // blad wywolany przez mailera
    const MAILER_ERROR = 5;
    
    // problem z utworzeniem wiadomosci
    const MESSAGE_COMPOSE_ERROR = 6;
}
