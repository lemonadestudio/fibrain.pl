<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Cms\ElmatBundle\Model\OfferType;
use Cms\ElmatBundle\Model\NewsletterMessageRecipientStatus;

/**
 *
 * @ORM\Table(name="newsletter_message")
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\NewsletterMessageRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class NewsletterMessage {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $plainTextMessage;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startSendDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     *
     * Newsletter zostanie wysłany tylko do subskrybentów, którzy
     * posiadają konto w systemie (sprawdzane na podtawie zgodności emaila).
     */
    private $sendOnlyToSubscribersWithAccount;

    /**
     * @ORM\OneToMany(
     *   targetEntity="NewsletterMessageOfferType",
     *   mappedBy="newsletterMessage",
     *   cascade={"persist", "remove"},
     *   orphanRemoval=true
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $offerTypes;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * Jeżeli to pole będzie puste, to newsletter zostanie wysłany do
     * wszystkich subskrybentów. W przeciwnym wypadku newsletter zostanie wysłany tylko
     * do subskrybentów, do których pasują adresy mailowe.
     */
    private $recipientEmails;

    /**
     * @ORM\OneToMany(
     *   targetEntity="NewsletterMessageRecipient",
     *   mappedBy="newsletterMessage",
     *   cascade={"persist", "remove"},
     *   orphanRemoval=true
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $recipients;

    // kontener na liste "id" klasy NewsletterSubscriber
    // potrzebne tylko do walidacji, bo sonata nie pozwala na walidacje
    // wlasciwosci formularza, ktore nie sa zmapowane do obiektu docelowego
    private $recipientIds;

    public function __construct() {
        $this->createdAt = new \DateTime();
    }

    public function __toString() {
        return '' . $this->title;
    }

    public function getShortMessage() {
        return substr($this->message, 0, 100);
    }

    public function getShortTitle() {
        return substr($this->title, 0, 100);
    }

    /**
     * Zapytania powoduja n+1. Mimo tego jest na liscie w adminie, bo trzeba wyswietlic jakies info.
     * Niestety sonata nie pozwala na pobranie tych danych w jednym zapytaniu.
     */
    public function getRecipientsCount() {
        return count($this->recipients);
    }

    public function getRecipientIds() {
        return $this->recipientIds;
    }

    public function setRecipientIds(array $recipientIds) {
        $this->recipientIds = $recipientIds;
    }

    // nie uzywac w listowaniu bo wyjda tragiczne zapytania (n+1)
    public function getRecipientsAsString() {
        $recipients = '';

        foreach ($this->getRecipients() as $recipient) {
            if (strlen($recipients) > 0) {
                $recipients .= ', ';
            }

            $recipients .= $recipient->getNewsletterSubscriber()->getEmail();
        }

        return $recipients;
    }

    // nie uzywac w listowaniu bo wyjda tragiczne zapytania (n+1)
    public function getStatus() {
        $sentCount = 0;
        $sentWithErrors = 0;
        $toSendCount = 0;
        $toResendCount = 0;
        $lastSentDate = null;

        foreach ($this->getRecipients() as $recipient) {
            if ($recipient->getSentDate() > $lastSentDate) {
                $lastSentDate = $recipient->getSentDate();
            }

            switch ($recipient->getStatus()) {
                case NewsletterMessageRecipientStatus::SENT:
                    $sentCount++;
                    break;
                case NewsletterMessageRecipientStatus::READY_TO_SEND:
                    $toSendCount++;
                    break;
                case NewsletterMessageRecipientStatus::MAILER_ERROR:
                case NewsletterMessageRecipientStatus::MESSAGE_COMPOSE_ERROR:
                    $toResendCount++;
                    break;
                case NewsletterMessageRecipientStatus::NO_ACCOUNT:
                case NewsletterMessageRecipientStatus::INVALID_EMAIL:
                    $sentWithErrors++;
                    break;
            }
        }

        return $this->getStatusText($sentCount, $sentWithErrors, $toSendCount, $toResendCount, $lastSentDate);
    }

    public function getOfferTypeNames() {
        $names = '';

        foreach ($this->getOfferTypes() as $offerType) {
            if (strlen($names) > 0) {
                $names .= ', ';
            }

            $names .= OfferType::getDescription($offerType->getType());
        }

        return $names;
    }

    public function getFormattedRecipientEmails() {
        $recipientEmails = $this->getRecipientEmails();
        $formatted = '';

        if (empty($recipientEmails)) {
            return $formatted;
        }

        foreach (explode("\n", $recipientEmails) as $email) {
            if (strlen($formatted) > 0) {
                $formatted .= ', ';
            }

            $formatted .= trim($email);
        }

        return $formatted;
    }

    public function getRecipientEmailsList() {
        $recipientEmails = $this->getRecipientEmails();
        $emailsList = array();

        if (empty($recipientEmails)) {
            return $emailsList;
        }

        foreach (explode("\n", $recipientEmails) as $email) {
            $emailsList[] = trim($email);
        }

        return $emailsList;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate() {
        $this->updatedAt = new \DateTime();
    }

    private function getStatusText($sentCount, $sentWithErrors, $toSendCount, $toResendCount, $lastSentDate) {
        $recipientsCount = $this->getRecipientsCount();

        if ($recipientsCount === $toSendCount) {
            return "Jeszcze nie rozpoczęto wysyłania.\n\r";
        }

        $status = "Wysłano {$sentCount}/{$recipientsCount} wiadomości.\n\r";

        if ($sentWithErrors > 0) {
            $status .= "Podczas wysyłania {$sentWithErrors} wiadomości wystąpił błąd (brak przypisanego konta użytkownika lub nieprawidłowy adres email)";
        }

        if ($toSendCount > 0) {
            $status .= "Do wysłania zostało {$toSendCount} wiadomości.\n\r";
        }

        if ($toResendCount > 0) {
            $status .= "Do ponownego wysłania zostało {$toResendCount} wiadomości (z powodu błędu tworzenia wiadomości lub błędu wysyłania emaila).";
        }

        if ($toSendCount + $toResendCount === 0 && $lastSentDate instanceof \DateTime) {
            $status .= "Wiadomość została wysłana do wszystkich odbiorców. Data zakończenia: {$lastSentDate->format('Y-m-d H:i:s')}.";
        }

        return $status;
    }

    public function getStartSendDateText() {
        if (!$this->getStartSendDate()) {
            return 'Natychmiast.';
        }

        return $this->getStartSendDate()->format('Y-m-d H:i:s');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return NewsletterMessage
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return NewsletterMessage
     */
    public function setMessage($message) {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Set plainTextMessage
     *
     * @param string $plainTextMessage
     * @return NewsletterMessage
     */
    public function setPlainTextMessage($plainTextMessage) {
        $this->plainTextMessage = $plainTextMessage;

        return $this;
    }

    /**
     * Get plainTextMessage
     *
     * @return string
     */
    public function getPlainTextMessage() {
        return $this->plainTextMessage;
    }

    /**
     * Set startSendDate
     *
     * @param \DateTime $startSendDate
     * @return NewsletterMessage
     */
    public function setStartSendDate($startSendDate) {
        $this->startSendDate = $startSendDate;

        return $this;
    }

    /**
     * Get startSendDate
     *
     * @return \DateTime
     */
    public function getStartSendDate() {
        return $this->startSendDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NewsletterMessage
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return NewsletterMessage
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set sendOnlyToSubscribersWithAccount
     *
     * @param boolean $sendOnlyToSubscribersWithAccount
     * @return NewsletterMessage
     */
    public function setSendOnlyToSubscribersWithAccount($sendOnlyToSubscribersWithAccount) {
        $this->sendOnlyToSubscribersWithAccount = $sendOnlyToSubscribersWithAccount;

        return $this;
    }

    /**
     * Get sendOnlyToSubscribersWithAccount
     *
     * @return boolean
     */
    public function getSendOnlyToSubscribersWithAccount() {
        return $this->sendOnlyToSubscribersWithAccount;
    }

    /**
     * Set recipientEmails
     *
     * @param string $recipientEmails
     * @return NewsletterMessage
     */
    public function setRecipientEmails($recipientEmails) {
        $this->recipientEmails = $recipientEmails;

        return $this;
    }

    /**
     * Get recipientEmails
     *
     * @return string
     */
    public function getRecipientEmails() {
        return $this->recipientEmails;
    }

    /**
     * Add offerTypes
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterMessageOfferType $offerTypes
     * @return NewsletterMessage
     */
    public function addOfferType(NewsletterMessageOfferType $offerTypes) {
        $this->offerTypes[] = $offerTypes;

        return $this;
    }

    /**
     * Remove offerTypes
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterMessageOfferType $offerTypes
     */
    public function removeOfferType(NewsletterMessageOfferType $offerTypes) {
        $this->offerTypes->removeElement($offerTypes);
    }

    /**
     * Get offerTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOfferTypes() {
        return $this->offerTypes;
    }

    /**
     * Add recipients
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterMessageRecipient $recipients
     * @return NewsletterMessage
     */
    public function addRecipient(NewsletterMessageRecipient $recipients) {
        $this->recipients[] = $recipients;

        return $this;
    }

    /**
     * Remove recipients
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterMessageRecipient $recipients
     */
    public function removeRecipient(NewsletterMessageRecipient $recipients) {
        $this->recipients->removeElement($recipients);
    }

    /**
     * Get recipients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecipients() {
        return $this->recipients;
    }
}