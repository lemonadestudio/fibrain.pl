<?php
namespace Cms\ElmatBundle\Entity;

use Cms\ElmatBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\RecycleBinRepository")
 * @ORM\Table(name="recycle_bin")
 * @ORM\HasLifecycleCallbacks()
 */
class RecycleBin {

    /**
     * @var Cms
     */
    private $helper = null;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @var string $item
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $item;

    /**
     * @var string $item_id
     *
     * @ORM\Column(type="integer");
     * @Assert\NotNull();
     */
    private $item_id;

    /**
     * @var string $from_entity
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $from_entity;
    
    /**
     * @var \DateTime $deleted_at
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $deleted_at;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return RecycleBin
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set kolejnosc
     *
     * @param string $kolejnosc
     * @return RecycleBin
     */
    public function setKolejnosc($kolejnosc)
    {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return string 
     */
    public function getKolejnosc()
    {
        return $this->kolejnosc;
    }

    /**
     * Set deleted_at
     *
     * @param \DateTime $deletedAt
     * @return RecycleBin
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deleted_at
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * Set item
     *
     * @param string $item
     * @return RecycleBin
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return string 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set item_id
     *
     * @param integer $itemId
     * @return RecycleBin
     */
    public function setItemId($itemId)
    {
        $this->item_id = $itemId;

        return $this;
    }

    /**
     * Get item_id
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * Set from_entity
     *
     * @param string $fromEntity
     * @return RecycleBin
     */
    public function setFromEntity($fromEntity)
    {
        $this->from_entity = $fromEntity;

        return $this;
    }

    /**
     * Get from_entity
     *
     * @return string 
     */
    public function getFromEntity()
    {
        return $this->from_entity;
    }
}
