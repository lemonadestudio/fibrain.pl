<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class SliderAdmin extends Admin {
	
//    protected $translationDomain = 'CmsElmatBundle';
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('title', null, array('required' => true))
                ->add('photos', null, array('required' => false))
                ->add('published', 'checkbox', array('required' => false))
                ->add('publishDate', 'datetime')
                ->add('lang', 'choice', array(
                    'choices' => array("pl"=>"Polski", "en"=>"English"),

                ));
           

    }
    

    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')

        ;
    }
    

}