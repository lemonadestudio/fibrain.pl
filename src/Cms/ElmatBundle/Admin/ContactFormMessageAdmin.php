<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ContactFormMessageAdmin extends Admin {

	protected $translationDomain = 'CmsElmatBundle';

	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'DESC', // sort direction
			'_sort_by' => 'sentAt' // field name
	);

	protected function configureRoutes(RouteCollection $collection) {

				$collection->remove('create');

				$collection->remove('edit');
// 				$collection->remove('batch');
// 				$collection->remove('show');
// 				$collection->remove('delete');

	}

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('senderName')
                ->add('senderEmail')
                ->add('senderMessage')
                ->add('sentAt')
                ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper->addIdentifier('sentAt')
        ->add('senderName')
        ->add('senderEmail')
        ->add('senderMessage', null, array('template' => 'CmsElmatBundle:Admin:ContactFormMessage\_message.html.twig'))
       ;

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
//         $datagridMapper
//             ->add('tag')

//         ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {
//         $formMapper

//              ->add('tag')



//         ;


    }



}