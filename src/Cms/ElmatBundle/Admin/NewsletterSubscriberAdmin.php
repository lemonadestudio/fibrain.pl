<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Cms\ElmatBundle\Entity\NewsletterSubscriber;

class NewsletterSubscriberAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC', // sort direction
        '_sort_by' => 'name' // field name
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->remove('show');
    }

    public function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('email')
                ->add('isActive')
                ->add('createdAt');

        $listMapper->add('_action', 'actions', array(
            $this->trans('actions') => array(
                'edit' => array(),
                'delete' => array()
            )
        ));
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('email')
                ->add('isActive');
    }

    public function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('email')
                ->add('isActive', null, array(
                    'required' => false
        ));
    }

}
