<?php

namespace Cms\ElmatBundle\Admin;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;

class UserAdmin extends BaseUserAdmin {

    protected function configureRoutes(RouteCollection $collection) {
        parent::configureRoutes($collection);
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) {
        if (!$childAdmin && !in_array($action, array(
                'edit',
            ))
        ) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('firma')
            ->add('groups')
            ->add('locked')
            ->add('roles');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->with('General')
            ->add('username')
            ->add('email')
            ->add('plainPassword', 'text', array('required' => false))
            ->end()
            ->with('Dane firmy')
            ->add('firma', null, array('required' => false, 'label' => 'Nazwa firmy'))
            ->add('ulica', null, array('required' => false, 'label' => 'Ulica, nr'))
            ->add('kod_pocztowy', null, array('required' => false, 'label' => 'Kod pocztowy'))
            ->add('miasto', null, array('required' => false, 'label' => 'Miasto'))
            ->add('wojewodztwo', 'choice', array('required' => false, 'choices' => \Cms\ElmatBundle\Entity\User::$wojewodztwa, 'label' => 'Województwo'))
            ->add('nip', null, array('required' => false, 'label' => 'NIP'))
            ->add('telefon', null, array('required' => false, 'label' => 'Tel.'))
            ->add('fax', null, array('required' => false, 'label' => 'Fax'))
            ->end()
            ->with('Groups')
            ->add('groups', 'sonata_type_model', array('required' => false, 'expanded' => true, 'multiple' => true))
            ->end()
            ->with('Profile')
            ->add('firstname', null, array('required' => false))
            ->add('lastname', null, array('required' => false))
            ->add('firma_isp', null, array('label' => 'ISP'))
            ->add('firma_tv_kablowa', null, array('label' => 'Operator telewizji kablowej'))
            ->add('firma_instalator', null, array('label' => 'Instalator sieci światłowodowych'))
            ->add('firma_instalator_sieci_niskopradowe', null, array('label' => 'Instalator sieci niskoprądowych'))
            ->add('firma_samorzad_terytorialny', null, array('label' => 'Samorząd terytorialny'))
            ->add('firma_handlowa', null, array('label' => 'Firma handlowa'))
            ->add('firma_administrator_sieci', null, array('label' => 'Administrator sieci'))
            ->add('firma_projektant', null, array('label' => 'Projektant/biuro projektowe'))
            ->add('firma_integrator', null, array('label' => 'Integrator'))
            ->add('firma_inny', null, array('label' => 'Inny (jaki)'))
            ->add('oferta_swiatlowody', null, array('label' => 'Kable światłowodowe'))
            ->add('oferta_akcesoria_swiatlowodowe', null, array('label' => 'Akcesoria światłowodowe'))
            ->add('oferta_mikrokanalizacja', null, array('label' => 'Mikrokanalizacja MetroJet'))
            ->add('oferta_kable_napowietrzne_airtrack', null, array('label' => 'Kable napowietrzne AirTrack'))
            ->add('oferta_technologia_fttx', null, array('label' => 'Technologia FTTx'))
            ->add('oferta_technologia_gpon', null, array('label' => 'Technologia GPON'))
            ->add('oferta_urzadzenia_aktywne', null, array('label' => 'Urządzenia aktywne'))
            ->add('oferta_sieci_miejskie', null, array('label' => 'Rozwiązania dla sieci miejskich'))
            ->add('oferta_osprzet_instalacyjny', null, array('label' => 'Osprzęt instalacyjny'))
            ->add('oferta_osprzet_pomiarowy', null, array('label' => 'Osprzęt pomiarowy'))
            ->add('oferta_okablowanie_strukturalne', null, array('label' => 'Okablowanie strukturalne'))
            ->add('oferta_inne', null, array('label' => 'Inne'))
            ->add('info_staly_klient', null, array('label' => 'Jestem stałym klientem'))
            ->add('info_wyszukiwarka', null, array('label' => 'Wyszukiwarka'))
            ->add('info_prasa_codzienna', null, array('label' => 'Prasa codzienna'))
            ->add('info_reklamy', null, array('label' => 'Reklamy w internecie'))
            ->add('info_prasa_specjalistyczna', null, array('label' => 'Prasa specjalistyczna'))
            ->add('info_znajomi', null, array('label' => 'Znajomi, z polecenia'))
            ->add('info_subskrypcja', null, array('label' => 'Subskrybcja ze strony'))
            ->add('info_inne', null, array('label' => 'Inne'))
            ->end();

        if (!$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->with('Management')
                ->add('roles', 'sonata_security_roles', array(
                    'expanded' => true,
                    'multiple' => true,
                    'required' => false
                ))
                ->add('locked', null, array('required' => false))
                ->add('expired', null, array('required' => false))
                ->add('enabled', null, array('required' => false))
                ->add('credentialsExpired', null, array('required' => false))
                ->end();
        }
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('username')
            ->add('email')
            ->add('dane', null, array('template' => 'CmsElmatBundle:Admin\UserAdmin:dane.html.twig'))
            ->add('groups')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
            ->add('createdAt');
    }
}
