<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;

class ProductPowiazanyAdmin extends Admin {


	protected $translationDomain = 'CmsElmatBundle';

	protected function configureRoutes(RouteCollection $collection)
	{

	}

	public function showIn($context) {
	    // return false;
	}



	public function getTemplate($name)
	{
	    switch ($name) {
// 	        case 'edit':
// 	           return 'CmsElmatBundle:Admin\\ProductZdjecie:edit.html.twig';
// 	            break;
	        default:
	            return parent::getTemplate($name);
	            break;
	    }
	}

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('kolejnosc')
                ->add('powiazany')





         ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper
        	->addIdentifier('id', null, array())
        	->add('product')
        	->add('powiazany')
        	->add('kolejnosc')



                ;

            $listMapper->add('_action', 'actions', array(
            		$this->trans('actions') => array(
            				'edit' => array(),
            				'delete' => array(),
            				'view' => array(),
            		)
            	)
			);

    }



    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
//              ->add('product')
        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

    	$this->getSubject()->setUpdatedAt( new \DateTime('now'));

        $formMapper
        	->add('kolejnosc')
        	->add('product')
        	->add('powiazany')

        ;

        $formMapper->setHelps(array(

        		));


    }

}