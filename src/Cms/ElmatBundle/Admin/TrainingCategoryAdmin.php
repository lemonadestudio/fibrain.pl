<?php

namespace Cms\ElmatBundle\Admin;

use Doctrine\ORM\EntityRepository;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TrainingCategoryAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\TrainingCategory:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {

        $collection->add('move_up', '{id}/move_up');
        $collection->add('move_down', '{id}/move_down');


    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        // rozważyć przeniesienie sortowania z productCategory (jeżeli będzie połączenie do OfferGroup)


        $_alias = $query->getRootAlias();

        $query->orderBy($_alias.'.kolejnosc', 'ASC');
        $query->addOrderBy($_alias.'.root', 'ASC');
        $query->addOrderBy($_alias.'.lft', 'ASC');

        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $this->getSubject()->setUpdated( new \DateTime('now'));
        $id = $this->getRequest()->get($this->getIdParameter());

        $formMapper
                ->add('title', null, array('required' => true))

                ->add('parent', null, array(
                        'property' => 'nametree',
                        'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('u')
                            ->orderBy('u.root', 'ASC')
                            ->addOrderBy('u.lft', 'ASC');},
                            'required' => false,
                            'label' => 'label.parent_category'))

                ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))

                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->add('_delete_file_image', 'checkbox', array('required'=> false, 'label' => 'Usunąć zdjęcie?'))

                ->add('automaticSeo', 'checkbox', array('required' => false))
                ->add('slug', null, array('required' => false))
                ->add('keywords', null, array('required' => false))
                ->add('description', 'textarea', array('required' => false))
                ->add('updated', null, array( 'with_seconds' => 'true', 'label' => ' ', 'attr'=>array('style'=>'display:none;')))
                ->add('color', null, array('required' => false))

           ;

        if($id) {
            $obj = $this->getObject($id);
            if($obj->getLvl() == 0) {
                $formMapper->add('kolejnosc');
            }
        }

        $formMapper
        -> setHelps(array(
//                 'slug' => $this->trans('help.slug'),
//                 'content_short' => 'Krótki opis widoczny na listowaniu kategorii obok zdjęcia',
                'offer_group' => 'Zmiana grupy ofertowej jest możliwa tylko dla kategorii na najwyższym poziomie. Po zmianie grupy dla nadrzędnej kategorii, grupa ofertowa zostanie ustawiona dla wszystkich elementów podrzędnych',
                'kolejnosc' => 'Kolejność - dotyczy tylko kategorii na najwyższym poziomie',
                'color' => 'Kod koloru akceptowany przez właściwość CSS background, np.: "green", "#ff00ff", "rgb(0,200,100)" '
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nametree')
            ->add('moveupdown', 'string', array('label' => 'Kolejność', 'template' => 'CmsElmatBundle:Admin\TrainingCategory:list_moveupdown.html.twig'))

            ->add('photo', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
                
            ->add('color')

            ->add('_action', 'actions', array(
                    'actions' => array(
                            // 'view' => array(),
                            'edit' => array(),
                            'delete' => array(),

                    )
            ))
                
        ;
    }

    public function postUpdate($object) {

        $this->postPersist($object);

    }

    public function postPersist($object)
    {

        $doctrine = $this->getConfigurationPool()->getContainer()->get('doctrine');

        // po aktualizacji kategorii aktualizacja grupy ofertowej dla całego drzewa kategorii

        $root = $object->getRoot();
        $lvl = $object->getLvl();

         $offer_group_set = false;
//         $offer_group = null;

        $update_kolejnosc = null;
        $kolejnosc = null;

        if($lvl > 0) {


            $root_object = $doctrine->getRepository('CmsElmatBundle:TrainingCategory')->findOneBy(array(
                    'lvl' => 0,
                    'root' => $root));

            if($root_object) {
//                 $offer_group = $root_object->getOfferGroup();
//                 $offer_group_set = true;

                $update_kolejnosc = true;
                $kolejnosc = $root_object->getKolejnosc();
            }

        } else {

//             $offer_group = $object->getOfferGroup();
//             $offer_group_set = true;

            $update_kolejnosc = true;
            $kolejnosc = $object->getKolejnosc();
        }




        // todo: jeżeli będzie powiązanie do OfferGroup
        if($offer_group_set) {

            $dql = "UPDATE CmsElmatBundle:TrainingCategory pc
                SET pc.offer_group = :offer_group
                WHERE pc.root = :root";

            $query = $doctrine->getManager()
            ->createQuery($dql)
            ->setParameter('root', $root)
            ->setParameter('offer_group', $offer_group);

            $query->execute();

        }

        if($update_kolejnosc) {

            $dql = "UPDATE CmsElmatBundle:TrainingCategory pc
                SET pc.kolejnosc  = :kolejnosc
                WHERE pc.root = :root";

            $query = $doctrine->getManager()
            ->createQuery($dql)
            ->setParameter('root', $root)
            ->setParameter('kolejnosc', $kolejnosc)
            ;

            $query->execute();

        }



    }


}