<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Cms\ElmatBundle\Entity\Newsletter;

class NewsletterAdmin extends Admin {

	protected $translationDomain = 'CmsElmatBundle';

// 	public function  showIn($context) {
// 		return false;
// 	}

	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'DESC', // sort direction
			'_sort_by' => 'data_wysylki' // field name
	);


	public function getTemplate($name)
	{
	    switch ($name) {
	        case 'edit':
	            return 'CmsElmatBundle:Admin\Newsletter:edit.html.twig';
	            break;
            case 'show':
                return 'CmsElmatBundle:Admin\Newsletter:show.html.twig';
                break;
	            // 			case 'show':
	            // 				return 'CmsElmatBundle:Admin\Aktualnosci:show.html.twig';
	            // 				break;

	        default:
	            return parent::getTemplate($name);
	            break;
	    }
	}


	protected function configureRoutes(RouteCollection $collection) {

// 				$collection->remove('create');
// 				$collection->remove('edit');
// 				$collection->remove('batch');
// 				$collection->remove('show');
// 				$collection->remove('delete');

	}


    public function configureListFields(ListMapper $listMapper) {



        $listMapper->addIdentifier('tytul')
        ->add('statusTxt')
        ->add('data_wysylki')
        ->add('count_sent')
        ->add('count_to_send')
        ->add('wyslij_testowy', null, array( 'template' => 'CmsElmatBundle:Admin\Newsletter:wyslij_testowy.html.twig'))
        ->add('tylko_z_kontem')



       ;

       $listMapper->add('_action', 'actions', array(
       		$this->trans('actions') => array(
       			    'view' => array(),
       				'edit' => array(),
//        				'delete' => array()
       		)
       ));

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('tytul')
            ->add('status', 'doctrine_orm_choice', array(), 'choice', array('choices' => Newsletter::getStatusChoices() ))
            ->add('tylko_z_kontem')

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {


		$formMapper
		->add('tytul')
		->add('status', 'choice', array('choices' => Newsletter::getStatusChoices() ))
		->add('data_wysylki')
		->add('tresc', null, array('attr' => array('class' => 'sonata-medium wysiwyg')))
		->add('tresc_txt', null, array('attr' => array('style' => 'width:800px',  'cols' => 220, 'rows' => 6)))
        ->add('tylko_z_kontem')
        ->add('lista_reczna')
                
        ->add('oferta_swiatlowody', null, array('label' => 'Kable światłowodowe'))
        ->add('oferta_akcesoria_swiatlowodowe', null, array('label' => 'Akcesoria światłowodowe'))
        ->add('oferta_mikrokanalizacja', null, array('label' => 'Mikrokanalizacja MetroJet'))
        ->add('oferta_kable_napowietrzne_airtrack', null, array('label' => 'Kable napowietrzne AirTrack'))
        ->add('oferta_technologia_fttx', null, array('label' => 'Technologia FTTx'))
        ->add('oferta_technologia_gpon', null, array('label' => 'Technologia GPON'))
        ->add('oferta_urzadzenia_aktywne', null, array('label' => 'Urządzenia aktywne'))
        ->add('oferta_sieci_miejskie', null, array('label' => 'Rozwiązania dla sieci miejskich'))
        ->add('oferta_osprzet_instalacyjny', null, array('label' => 'Osprzęt instalacyjny'))
        ->add('oferta_osprzet_pomiarowy', null, array('label' => 'Osprzęt pomiarowy'))
        ->add('oferta_okablowanie_strukturalne', null, array('label' => 'Okablowanie strukturalne'))

		->setHelps(array(
		        'tytul' => 'Tytuł maila',
		        'tresc' => 'Treść maila wersja HTML<br />Dostępne zmienne:<br /> - <em>{URL_REZYGNACJA}</em> - link do wypisania się z newsletera',
		        'tresc_txt' => 'Treść maila - wersja dla klientów obsługujących tylko tryb tekstowy. Uzupełnienie nie jest konieczne, ale zapewnia kompatybilność z klientami tekstowymi.',
                'tylko_z_kontem' => 'Newsletter wysyłany tylko do użytkowników posiadających konto z tym samym adresem email. '
                                    . 'Po wybraniu tej opcji dostępne są nowe zmienne w szablonie:<br />'
                                    . ' - <em>{URL_ZMIANA_HASLA}</em> - link do resetowania hasła <br /> '
                                    . ' - <em>{FIRMA}</em> - nazwa firmy<br />'
                                    . ' - <em>{NIP}</em> - nip<br />'
                                    . ' - <em>{EMAIL}</em> - email<br />'
                                    . ' - <em>{TELEFON}</em> - tel.<br />'
                                    . ' - <em>{ULICA}, {KOD_POCZTOWY}, {MIASTO}</em> - elementy adresu firmy<br />',
                'lista_reczna' => 'Manualna lista adresów email (w nowych linijkach) na które ma zostać wysłany Newsletter. 
                    <br>UWAGA: Newsletter zostanie wysłany na dany adres email, tyklo w przypadku gdy dany adres jest dodany jako subskrypcja.'
                                    
            ))

		;





    }

    public function getNewInstance()
    {
        $object = parent::getNewInstance();


        $def_template = $this->configurationPool->getContainer()->get('cms_email_templates')->get('newsletter_domyslny_szablon');
        if($def_template) {
            $object->setTytul($def_template->getTopic());
            $object->setTresc($def_template->getContent());
           //  $object->setTrescTxt($def_template->getContentTxt());
        }

        return $object;

    }






}