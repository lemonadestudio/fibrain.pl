<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class TrainingCityAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

                ->add('title', null, array('required' => true))


        -> setHelps(array(


           ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')


        ;
    }


}