<?php

namespace Cms\ElmatBundle\Controller;

use Cms\ElmatBundle\Repository\PageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller {
    /**
     * @return PageRepository
     */
    public function getPageRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:Page');
    }

    /**
     * Finds and displays a Page entity.
     * @param $slug
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction($slug, $id) {
        $page = $this->getPageRepository()->findOneBy(array('id' => $id));

        if (!$page) {
            return $this->redirect("/");
        }

        if ($page->getSlug() != $slug) {
            return $this->redirect($this->get('router')->generate('cms_elmat_page_show', array('id' => $page->getId(), 'slug' => $page->getSlug())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Strona główna", "/");
        $breadcrumbs->addItem($page->getTitle(), $this->get('router')->generate('cms_elmat_page_show', array('id' => $page->getId(), 'slug' => $page->getSlug())));

        return $this->render('CmsElmatBundle:Page:show.html.twig', array(
            'page' => $page
        ));
    }
}
