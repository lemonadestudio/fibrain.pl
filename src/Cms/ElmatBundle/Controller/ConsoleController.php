<?php

namespace Cms\ElmatBundle\Controller;

use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\StreamOutput;

class ConsoleController extends Controller {
    public function assetsInstallAction($guid) {
        if ($guid !== 'eff16048-b786-4a45-a4f8-8f389f5c73ca') {
            throw $this->createNotFoundException('GUID not recognized.');
        }

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'assets:install',
            'target' => '../web',
            '--symlink' => true
        ));

        // You can use NullOutput() if you don't need the output
        $output = new StreamOutput(tmpfile(), StreamOutput::VERBOSITY_NORMAL, true);
        $application->run($input, $output);

        // return the output
        $converter = new AnsiToHtmlConverter();
        rewind($output->getStream());
        $content = stream_get_contents($output->getStream());
        fclose($output->getStream());

        $html = $converter->convert($content);

        return $this->render('CmsElmatBundle:Console:index.html.twig', array(
            'html' => $html
        ));
    }

    public function doctrineMigrationsMigrateAction($guid) {
        if ($guid !== 'f24d63bf-e4a5-4f2b-b57a-9286cdb20d69') {
            throw $this->createNotFoundException('GUID not recognized.');
        }

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'doctrine:migrations:migrate'
        ));

        // You can use NullOutput() if you don't need the output
        $output = new StreamOutput(tmpfile(), StreamOutput::VERBOSITY_NORMAL, true);
        $application->run($input, $output);

        // return the output
        $converter = new AnsiToHtmlConverter();
        rewind($output->getStream());
        $content = stream_get_contents($output->getStream());
        fclose($output->getStream());

        $html = $converter->convert($content);

        return $this->render('CmsElmatBundle:Console:index.html.twig', array(
            'html' => $html
        ));
    }
}
