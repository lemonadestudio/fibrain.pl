<?php

namespace Cms\ElmatBundle\Controller\Admin;


use Cms\ElmatBundle\Helper\Cms;

use Symfony\Bundle\DoctrineBundle\Registry;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class MenuItemAdminController extends Controller
{

	public function saveOrderAction() {

		$request = $this->getRequest();
		$elements = $request->get('elements');

		$em = $this->getDoctrine()->getManager();



		$menu_repo = $em->getRepository('CmsElmatBundle:MenuItem');

		$order = 0;
		if(is_array($elements)) {
			foreach($elements as $element) {
				$menu_item = $menu_repo -> find($element['item_id']);


				if($menu_item) {
					$menu_item->setLft($element['left']);
					$menu_item->setRgt($element['left']);
					$menu_item->setDepth($element['depth']);

					$parent = $menu_repo->find($element['parent_id']);

					if(!$parent || $element['parent_id'] == 'root') {
						//$menu_item -> setAsRoot();
					} else {
						$menu_item -> setParentItem($parent);
					}
				}

				$em->flush();
			}


		}

		$response = new Response();
		$response->setContent('ok');



		return $response;

	}


	public function routeParametersAction() {

		$route = $this->getRequest()->request->get('route');
		$search = $this->getRequest()->request->get('search');

		$services = $this->admin->getSearchServices();


		$html = '';

		if(array_key_exists($route, $services)) {
			$service = $services[$route];
			if($service != null) {
				if($this->container->has($service));
				$elements = $this->container->get($service)->search($search);


				if(is_array($elements)) {
					$html = $this->get('templating')->render('CmsElmatBundle:CRUD:menu_routeparameters_choice.html.twig', array('elements' => $elements));
				}


			}
		}

		$response = new Response();
		$response->setContent($html);

		return $response;

	}



	/**
	 * TODO: zoptymalizować listę do 1 zapytania
	 *
	 * @return Response
	 */
	public function listAction()
	{
		if (false === $this->admin->isGranted('LIST')) {
			throw new AccessDeniedException();
		}

		$datagrid = $this->admin->getDatagrid();
		$formView = $datagrid->getForm()->createView();

		// set the theme for the current Admin Form
		// $this->get('twig')->getExtension('form')->setTheme($formView, $this->admin->getFilterTheme());


		$menu_cfg = $this->container->getParameter('cms_elmat.menu');
//                echo "<pre>";
//                print_r($menu_cfg);
//                echo "</pre>";

		$menus_tmp = $menu_cfg['locations'];

		$menus = array();

		$og_access = $this->getOfferGroupsAccess();

		foreach($menus_tmp as $_k => $_m) {
		    $_match = array();
            if(preg_match('/menu_gorne_(.*)/', $_k, $_match)) {
                if(in_array($_match[1], $og_access['symbols2'])) {
                    $menus[$_k] = $_m;
                }
            }

            if($og_access['grupa_elmat'] == true) {

                if($_k == 'menu_gorne1') {
                    $menus[$_k] = $_m;
                }

                if($_k == 'bottom_menu1') {
                    $menus[$_k] = $_m;
                }

            }

		}

		foreach( $menus as $loc_name => $location) {
			$menus[$loc_name]['menuItems'] = $this->getDoctrine()->getManager()->createQuery("
				SELECT i FROM CmsElmatBundle:MenuItem i
				WHERE 	i.location = :location
					AND ( i.depth = 1 OR i.depth IS NULL)
				ORDER BY i.location ASC, i.lft ASC
			")
			->setParameter('location', $loc_name)
			->execute();
		}

//                echo "<pre>";
//                print_r($menus);
//                echo "</pre>";


		return $this->render($this->admin->getTemplate('list'), array(
				'action'   => 'list',
				'menus'		=> $menus,

// 				'form'     => $formView,
// 				'datagrid' => $datagrid
		));
	}


	private function getOfferGroupsAccess() {




	    $s = $this->container->get('security.context');

	    $offer_groups = Cms::getOfferGroups();
	    $symbols2 = array('__');
	    $slugs = array('__');
	    foreach($offer_groups as $og) {
	        if($s->isGranted($og['role'])) {
	            $slugs[] = $og['slug'];
	            $symbols2[] = $og['symbol2'];
	        }
	    }

	    return array(
	            'slugs' => $slugs,
	            'symbols2' => $symbols2,
	            'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
	    );
	}



}
