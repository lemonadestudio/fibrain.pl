<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

class TrainingCategoryAdminController extends Controller
{


    public function moveUpAction($id = null) {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        $_repo = $this->getDoctrine()->getRepository('CmsElmatBundle:TrainingCategory');

        if($object->getLvl() > 0) {
            $_repo->moveUp($object);
            //    $this->get('session')->setFlash('sonata_flash_success', 'pozycję przeniesiono w górę');
        }



        $request=  $this->get('request');
        $referer = $request->headers->get('referer');
        if(!$referer) {
            $referer = $this->get('router')->generate('admin_cms_elmat_trainingcategory_list');
        }

        return $this->redirect($referer);



    }

    public function moveDownAction($id = null) {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        $_repo = $this->getDoctrine()->getRepository('CmsElmatBundle:TrainingCategory');

        if($object->getLvl() > 0) {
            $_repo->moveDown($object);
            //$this->get('session')->setFlash('sonata_flash_success', 'pozycję przeniesiono w dół');
        }



        $request=  $this->get('request');
        $referer = $request->headers->get('referer');
        if(!$referer) {
            $referer = $this->get('router')->generate('admin_cms_elmat_trainingcategory_list');
        }

        return $this->redirect($referer);


    }


}
